#include <stdio.h>
#include "euler1helper.c"
#include "../minunit.h"

int tests_run = 0;

int sumOfIsDivisableBy(int limit);

static char *test_sumOfIsDivisableBy()
{
    mu_assert("error, sum of all natural under 10 doesn't equal 23",
    sumOfIsDivisableBy(10) == 23);
    return 0;
}

static char *all_tests()
{
    mu_run_test(test_sumOfIsDivisableBy);
    return 0;
}

int main(int argc, char **argv)
{
    if (argc < 2){
        printf("You dun goofed, gimme an integer\n");
        return 1;
    }
    const int limit = atoi(argv[1]);
    char *result = all_tests();

    if (result != 0)
    {
        printf("%s\n", result);
    }
    else
    {
        printf("ALL TESTS PASSED\n");
    }
    printf("Tests run: %d\n", tests_run);

    printf("%d\n", sumOfIsDivisableBy(limit));

    return result != 0;
}

int sumOfIsDivisableBy(int limit)
{
    int total = 0;
    int i;
    for (i = 1; i < limit; i++)
    {
        int ya = isDivisableBy3or5(i);
        // printf("isDivisableBy3or5 %d = %d\n", i, ya);
        if (ya) total += i;
    }
    return total;
}