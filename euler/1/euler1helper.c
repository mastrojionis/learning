#ifndef bool
  #include <stdbool.h>
#endif

bool isDivisableBy3or5(int i){
  return i % 3 == 0 || i % 5 == 0;
}