#include <stdio.h>
// #include "minunit.h"

void fib(unsigned int x[2],unsigned  int *total)
{
  printf("%d\n", x[1]);
  if(x[1] % 2 == 0)
    *total += x[1];
  unsigned int newPrevious = x[1];
  x[1] += x[0];
  x[0] = newPrevious;
}

int main(int argc, char **argv)
{
    const unsigned int limit = 2047483647;
    unsigned int sum = 0;
    unsigned int previous = 1;
    unsigned int current = 2;

    // while (current < limit)
    // {
    //     printf("%d\n", current);
    //     if(current % 2 ==0)
    //       sum += current;
    //     int newPrevious = current;
    //     current += previous;
    //     previous = newPrevious;
    // }
    // printf("-----\n");
    // printf("%d\n", sum);

    sum = 0;
    int j[2] = {1,2};
    while(j[1] < limit){
      fib(j, &sum);
    }
    printf("-----\n");
    printf("%d\n", sum);
    return 0;
}